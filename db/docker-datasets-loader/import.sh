#!/bin/sh

set -e

PSQL_STRING="postgresql://${POSTGRES_USER}:${POSTGRES_PASS}@${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DBNAME}"

psql "${PSQL_STRING}" -c "CREATE SCHEMA IF NOT EXISTS external"

echo "Importing: bâtiments cadastre"
ogr2ogr \
	-f "PostgreSQL" "${PSQL_STRING}" \
	-lco OVERWRITE=YES \
	-lco GEOMETRY_NAME=geom \
	-nln external.cadastre_batiments \
	-t_srs EPSG:${SRID} -progress \
	/vsigzip//home/settings/cadastre_batiments.geojson.gz

echo "Importing: routes BD Topo"
ogr2ogr \
	-f "PostgreSQL" "${PSQL_STRING}" \
	-lco OVERWRITE=YES \
	-lco GEOMETRY_NAME=geom \
	-sql "SELECT * FROM troncon_de_route" \
	-nln external.bdtopo_routes \
	-t_srs EPSG:${SRID} -progress \
	/vsi7z//home/settings/bdtopo.7z/BDTOPO_3-3_TOUSTHEMES_GPKG_LAMB93_D035_2023-03-15/BDTOPO/1_DONNEES_LIVRAISON_2023-03-00245/BDT_3-3_GPKG_LAMB93_D035-ED2023-03-15/BDT_3-3_GPKG_LAMB93_D035-ED2023-03-15.gpkg

echo "All external datasets imported"
