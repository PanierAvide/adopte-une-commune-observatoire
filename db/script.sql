CREATE SCHEMA stats;

CREATE INDEX IF NOT EXISTS osm.osm_boundary_admin_level_idx ON osm.osm_boundary(admin_level);


----------------------------------------
-- Buildings
--

-- Comparison
CREATE MATERIALIZED VIEW stats.buildings AS
WITH o AS (
	SELECT
		b.id,
		SUM(ST_Area(o.geom)) AS area
	FROM osm.osm_boundary b
	LEFT JOIN osm.osm_buildings o ON b.geom && o.geom AND ST_Intersects(b.geom, o.geom)
	WHERE b.admin_level = 8
	GROUP BY b.id
),
c AS (
	SELECT
		b.id,
		SUM(ST_Area(c.geom)) AS area
	FROM osm.osm_boundary b
	LEFT JOIN external.cadastre_batiments c ON b.geom && c.geom AND ST_Intersects(b.geom, c.geom)
	WHERE b.admin_level = 8
	GROUP BY b.id
)
SELECT
	b.id,
	o.area AS area_osm, c.area AS area_cadastre,
	round(o.area / c.area) * 100 AS completion
FROM osm.osm_boundary b
LEFT JOIN o USING(id)
LEFT JOIN c USING(id)
WHERE b.admin_level = 8;

CREATE INDEX stats_buildings_id_idx ON stats.buildings(id);


----------------------------------------
-- Roads
--

-- BD Topo clean-up
DELETE FROM external.bdtopo_routes
WHERE
	fictif
	OR prive
	OR etat_de_l_objet = 'En projet'
	OR acces_vehicule_leger IN ('Physiquement impossible', 'Restreint aux ayants droit')
	OR nature IN ('Chemin', 'Sentier', 'Escalier');

REINDEX TABLE external.bdtopo_routes;

-- Comparison
CREATE MATERIALIZED VIEW stats.roads AS
WITH o AS (
	SELECT
		b.id,
		SUM(ST_Length(ST_Intersection(b.geom, o.geom))) AS length
	FROM osm.osm_boundary b
	LEFT JOIN osm.osm_roads o ON b.geom && o.geom AND ST_Intersects(b.geom, o.geom)
	WHERE b.admin_level = 8
	GROUP BY b.id
), t AS (
	SELECT
		b.id,
		SUM(ST_Length(ST_Intersection(b.geom, t.geom))) AS length
	FROM osm.osm_boundary b
	LEFT JOIN external.bdtopo_routes t ON b.geom && t.geom AND ST_Intersects(b.geom, t.geom)
	WHERE b.admin_level = 8
	GROUP BY b.id
)
SELECT
	b.id,
	o.length AS length_osm, t.length AS length_bdtopo,
	round(o.length / t.length) * 100 AS completion
FROM osm.osm_boundary b
LEFT JOIN o USING(id)
LEFT JOIN t USING(id)
WHERE b.admin_level = 8;

CREATE INDEX stats_roads_id_idx ON stats.roads(id);

-- ~ SELECT *
-- ~ FROM osm.osm_boundary b
-- ~ JOIN stats.roads s USING(id);
