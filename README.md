# Adopte une commune - Observatoire

Ceci est le dépôt contenant le code source de l'outil "Observatoire" du projet "Adopte une commune".

Il a vocation à afficher des statistiques d'avancement de la cartographie des routes et bâtiments dans OpenStreetMap.

## Mise en route

### Base de données

```bash
cd db/

# Download base datasets
cd settings
wget https://download.openstreetmap.fr/extracts/europe/france.osm.pbf -O country.pbf

depts=("01" "02" "03" "04" "05" "06" "07" "08" "09" "10" "11" "12" "13" "14" "15" "16" "17" "18" "19" "21" "22" "23" "24" "25" "26" "27" "28" "29" "2A" "2B" "30" "31" "32" "33" "34" "35" "36" "37" "38" "39" "40" "41" "42" "43" "44" "45" "46" "47" "48" "49" "50" "51" "52" "53" "54" "55" "56" "57" "58" "59" "60" "61" "62" "63" "64" "65" "66" "67" "68" "69" "70" "71" "72" "73" "74" "75" "76" "77" "78" "79" "80" "81" "82" "83" "84" "85" "86" "87" "88" "89" "90" "91" "92" "93" "94" "95" "971" "972" "973" "974" "976")
for d in "${depts[@]}"; do
	wget "https://cadastre.data.gouv.fr/data/etalab-cadastre/2023-04-01/geojson/departements/$d/cadastre-$d-batiments.json.gz"
done

bdtopoparts=("001" "002" "003" "004" "005" "006" "007" "008")
for p in "${bdtopoparts[@]}"; do
	wget "https://wxs.ign.fr/859x8t863h6a09o9o6fy4v60/telechargement/prepackage/BDTOPOV3-TOUSTHEMES-FRA-PACK_231$BDTOPO_3-3_TOUSTHEMES_SQL_WGS84G_FRA_2023-03-15/file/BDTOPO_3-3_TOUSTHEMES_SQL_WGS84G_FRA_2023-03-15.7z.$p"
done

# Launch database
docker compose up --build
```

## Licence

Copyright (c) Adrien PAVIE 2023

Released under the AGPL v3 terms, see the [LICENSE](LICENSE.txt) file to read the full text.
